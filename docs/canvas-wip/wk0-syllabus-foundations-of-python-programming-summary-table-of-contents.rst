Syllabus: Intro Python Programming
==================================

**Name:** Computer and Information Sciences (CISC) 179 - Programming

**Course:** Computer and Information Sciences (CISC) 179 - CRN 91377

**Instructor:** Hobson Lane

**E-mail:** lane@totalgood.com

Description
~~~~~~~~~~~

This is an introductory course in Python programming, incorporating the
fundamentals of object oriented software and user interface design. You
will learn how to program a computer to interact with computer input and
output devices to create a user interface for controlling that program.
You will learn how to:

-  Analyze user needs and requirements
-  Design a user interface for a Python application
-  Create classes with attributes and methods for implementing your
   program
-  Write software for event procedures and business logic
-  Test and debug the completed programs and applications
-  Document your program for users and developers using Python
   “doctests”

This course is intended for Computer and Information Sciences majors or
anyone interested in the Python programming language.

Important dates
~~~~~~~~~~~~~~~

-  01/29/2024 - Start of Spring Semester
-  02/09/2024 - Drop deadline (drops with full refund and no “W” on
   transcript)
-  02/16/2024 - 02/19/2024 - Lincoln/Washington Day (CAMPUS CLOSED)
-  03/25/2024 - 03/29/2024 - (SPRING BREAK)
-  03/29/2024 - Cesar Chavez Day (CAMPUS CLOSED)
-  04/12/2024 - Withdrawal Deadline (Primary 16-Wk Session)
-  04/30/2024 - Deadline to Apply for Graduation
-  05/25/2024 - End of Spring Semester

Learning goals
~~~~~~~~~~~~~~

1. Use conditional boolean expressions to control program flow.
2. Use ``if``\ …\ ``else`` structures to solve a problem.
3. Use a ``for`` or ``while`` loop to perform a sequence of actions.
4. Use predefined (hard-coded) ``list``, ``str``, ``float``, ``dict``,
   ``bool``, and ``int`` variables to contol program flow (data-driven
   software).
5. Use ``input`` and ``output`` to interact with the user through the
   console (terminal).
6. Use ``open``, ``read``, amd ``write`` to read and write strings to a
   file.
7. Design and implement a Python program that solves a problem using 1-6
   above.

Interactive Textbook: *Fundamentals of Python Programming*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Important sections from the textbook *Fundamentals of Python
Programming* on Runestone.

-  `1. General
   Introduction <https://runestone.academy/ns/books/published/mesa_python/GeneralIntro/toctree.html>`__

   -  `1.2.
      Algorithms <https://runestone.academy/ns/books/published/mesa_python/GeneralIntro/Algorithms.html>`__
   -  `1.3. The Python Programming
      Language <https://runestone.academy/ns/books/published/mesa_python/GeneralIntro/ThePythonProgrammingLanguage.html>`__
   -  `1.6. Formal and Natural
      Languages <https://runestone.academy/ns/books/published/mesa_python/GeneralIntro/FormalandNaturalLanguages.html>`__
   -  `1.7. A Typical First
      Program <https://runestone.academy/ns/books/published/mesa_python/GeneralIntro/ATypicalFirstProgram.html>`__
   -  `1.8. Predict Before You
      Run! <https://runestone.academy/ns/books/published/mesa_python/GeneralIntro/WPPredictBeforeYouRun.html>`__
      <== **ACTIVE LEARNING!!**

-  `2. Variables, Statements, and
   Expressions <https://runestone.academy/ns/books/published/mesa_python/SimplePythonData/toctree.html>`__

   -  `2.2. Values and Data
      Types <https://runestone.academy/ns/books/published/mesa_python/SimplePythonData/Values.html>`__
   -  `2.3. Operators and
      Operands <https://runestone.academy/ns/books/published/mesa_python/SimplePythonData/Operators.html>`__
   -  `2.4. Function
      Calls <https://runestone.academy/ns/books/published/mesa_python/SimplePythonData/FunctionCalls.html>`__
   -  `2.7.
      Variables <https://runestone.academy/ns/books/published/mesa_python/SimplePythonData/Variables.html>`__
   -  `2.10. Statements and
      Expressions <https://runestone.academy/ns/books/published/mesa_python/SimplePythonData/StatementsandExpressions.html>`__
   -  `2.11. Order of
      Operations <https://runestone.academy/ns/books/published/mesa_python/SimplePythonData/OrderofOperations.html>`__
   -  `2.13. Updating
      Variables <https://runestone.academy/ns/books/published/mesa_python/SimplePythonData/UpdatingVariables.html>`__

-  `3.
   Debugging <https://runestone.academy/ns/books/published/mesa_python/Debugging/toctree.html>`__

   -  `3.3. 👩🏾🖥️
      Debugging <https://runestone.academy/ns/books/published/mesa_python/Debugging/intro-HowtobeaSuccessfulProgrammer.html#debugging>`__
   -  `3.8. 👩🏾🖥️ Know Your Error
      Messages <https://runestone.academy/ns/books/published/mesa_python/Debugging/KnowyourerrorMessages.html>`__

-  `4. Python
   Modules <https://runestone.academy/ns/books/published/mesa_python/PythonModules/toctree.html>`__

   -  `4.2.
      Modules <https://runestone.academy/ns/books/published/mesa_python/PythonModules/intro-ModulesandGettingHelp.html>`__
   -  `4.3. The ``random``
      module <https://runestone.academy/ns/books/published/mesa_python/PythonModules/Therandommodule.html>`__

-  `5. Python
   Turtle <https://runestone.academy/ns/books/published/mesa_python/PythonTurtle/toctree.html>`__

   -  `5.3. Instances: A Herd of
      Turtles <https://runestone.academy/ns/books/published/mesa_python/PythonTurtle/InstancesAHerdofTurtles.html>`__
   -  `5.4. Object Oriented
      Concepts <https://runestone.academy/ns/books/published/mesa_python/PythonTurtle/ObjectInstances.html>`__
   -  `5.5. Repetition with a For
      Loop <https://runestone.academy/ns/books/published/mesa_python/PythonTurtle/RepetitionwithaForLoop.html>`__
   -  `5.8. 👩🏾🖥️ Incremental
      Programming <https://runestone.academy/ns/books/published/mesa_python/PythonTurtle/WPIncrementalProgramming.html>`__
      <== **Agile!!**

-  `6.
   Sequences <https://runestone.academy/ns/books/published/mesa_python/Sequences/toctree.html>`__

   -  `6.2. Strings and
      Lists <https://runestone.academy/ns/books/published/mesa_python/Sequences/StringsandLists.html>`__
   -  `6.3. Index Operator: Working with the Characters of a
      String <https://runestone.academy/ns/books/published/mesa_python/Sequences/IndexOperatorWorkingwiththeCharactersofaString.html>`__
   -  `6.6. The Slice
      Operator <https://runestone.academy/ns/books/published/mesa_python/Sequences/TheSliceOperator.html>`__
   -  `6.7. Concatenation and
      Repetition <https://runestone.academy/ns/books/published/mesa_python/Sequences/ConcatenationandRepetition.html>`__
   -  `6.8. Count and
      Index <https://runestone.academy/ns/books/published/mesa_python/Sequences/CountandIndex.html>`__
   -  `6.9. Splitting and Joining
      Strings <https://runestone.academy/ns/books/published/mesa_python/Sequences/SplitandJoin.html>`__

-  `7.
   Iteration <https://runestone.academy/ns/books/published/mesa_python/Iteration/toctree.html>`__

   -  `7.2. The for
      Loop <https://runestone.academy/ns/books/published/mesa_python/Iteration/TheforLoop.html>`__
   -  `7.5. Lists and ``for``
      loops <https://runestone.academy/ns/books/published/mesa_python/Iteration/Listsandforloops.html>`__
   -  `7.6. The Accumulator
      Pattern <https://runestone.academy/ns/books/published/mesa_python/Iteration/TheAccumulatorPattern.html>`__
   -  `7.8. Nested Iteration: Image
      Processing <https://runestone.academy/ns/books/published/mesa_python/Iteration/NestedIterationImageProcessing.html>`__
   -  `7.9. 👩🏾🖥️ Printing Intermediate
      Results <https://runestone.academy/ns/books/published/mesa_python/Iteration/WPPrintingIntermediateResults.html>`__

-  `8.
   Conditionals <https://runestone.academy/ns/books/published/mesa_python/Conditionals/toctree.html>`__

   -  `8.2. Boolean Values and Boolean
      Expressions <https://runestone.academy/ns/books/published/mesa_python/Conditionals/BooleanValuesandBooleanExpressions.html>`__
   -  `8.4. The ``in`` and ``not in``
      operators <https://runestone.academy/ns/books/published/mesa_python/Conditionals/Theinandnotinoperators.html>`__
   -  `8.5. Precedence of
      Operators <https://runestone.academy/ns/books/published/mesa_python/Conditionals/PrecedenceofOperators.html>`__
   -  `8.7. Omitting the ``else`` Clause: Unary
      Selection <https://runestone.academy/ns/books/published/mesa_python/Conditionals/OmittingtheelseClauseUnarySelection.html>`__
   -  `8.8. Nested
      conditionals <https://runestone.academy/ns/books/published/mesa_python/Conditionals/Nestedconditionals.html>`__
   -  `8.10. The Accumulator Pattern with
      Conditionals <https://runestone.academy/ns/books/published/mesa_python/Conditionals/TheAccumulatorPatternwithConditionals.html>`__

-  `9. Transforming
   Sequences <https://runestone.academy/ns/books/published/mesa_python/TransformingSequences/toctree.html>`__

   -  `9.3. List Element
      Deletion <https://runestone.academy/ns/books/published/mesa_python/TransformingSequences/ListDeletion.html>`__
   -  `9.4. Objects and
      References <https://runestone.academy/ns/books/published/mesa_python/TransformingSequences/ObjectsandReferences.html>`__
   -  `9.7. Mutating
      Methods <https://runestone.academy/ns/books/published/mesa_python/TransformingSequences/MutatingMethods.html>`__
   -  `9.8. Append versus
      Concatenate <https://runestone.academy/ns/books/published/mesa_python/TransformingSequences/AppendversusConcatenate.html>`__
   -  `9.11. The Accumulator Pattern with
      Strings <https://runestone.academy/ns/books/published/mesa_python/TransformingSequences/TheAccumulatorPatternwithStrings.html>`__
   -  `9.13. 👩🏾🖥️ Don’t Mutate A List That You Are Iterating
      Through <https://runestone.academy/ns/books/published/mesa_python/TransformingSequences/WPDontMutateAListYouIterateThrough.html>`__

-  `10.
   Files <https://runestone.academy/ns/books/published/mesa_python/Files/toctree.html>`__

   -  `10.2. Reading a
      File <https://runestone.academy/ns/books/published/mesa_python/Files/ReadingaFile.html>`__
   -  `10.3. Alternative File Reading
      Methods <https://runestone.academy/ns/books/published/mesa_python/Files/AlternativeFileReadingMethods.html>`__
   -  `10.4. Iterating over lines in a
      file <https://runestone.academy/ns/books/published/mesa_python/Files/Iteratingoverlinesinafile.html>`__
   -  `10.5. Finding a File in your
      Filesystem <https://runestone.academy/ns/books/published/mesa_python/Files/FindingaFileonyourDisk.html>`__
   -  `10.6. Using ``with`` for
      Files <https://runestone.academy/ns/books/published/mesa_python/Files/With.html>`__
   -  `10.7. Recipe for Reading and Processing a
      File <https://runestone.academy/ns/books/published/mesa_python/Files/FilesRecipe.html>`__
   -  `10.8. Writing Text
      Files <https://runestone.academy/ns/books/published/mesa_python/Files/WritingTextFiles.html>`__
   -  `10.10. Reading in data from a CSV
      File <https://runestone.academy/ns/books/published/mesa_python/Files/ReadingCSVFiles.html>`__

-  `11.
   Dictionaries <https://runestone.academy/ns/books/published/mesa_python/Dictionaries/toctree.html>`__

   -  `11.2. Getting Started with
      Dictionaries <https://runestone.academy/ns/books/published/mesa_python/Dictionaries/intro-Dictionaries.html>`__
   -  `11.5. Aliasing and
      copying <https://runestone.academy/ns/books/published/mesa_python/Dictionaries/Aliasingandcopying.html>`__
   -  `11.8. Accumulating the Best
      Key <https://runestone.academy/ns/books/published/mesa_python/Dictionaries/AccumulatingtheBestKey.html>`__

-  `12.
   Functions <https://runestone.academy/ns/books/published/mesa_python/Functions/toctree.html>`__

   -  `12.2. Function
      Definition <https://runestone.academy/ns/books/published/mesa_python/Functions/FunctionDefinitions.html>`__
   -  `12.4. Function
      Parameters <https://runestone.academy/ns/books/published/mesa_python/Functions/FunctionParameters.html>`__
   -  `12.5. Returning a value from a
      function <https://runestone.academy/ns/books/published/mesa_python/Functions/Returningavaluefromafunction.html>`__
   -  `12.9. Variables and parameters are
      local <https://runestone.academy/ns/books/published/mesa_python/Functions/Variablesandparametersarelocal.html>`__
   -  `12.10. Global
      Variables <https://runestone.academy/ns/books/published/mesa_python/Functions/GlobalVariables.html>`__
   -  `12.13. 👩🏾🖥️ Print
      vs. return <https://runestone.academy/ns/books/published/mesa_python/Functions/Printvsreturn.html>`__
   -  `12.14. Passing Mutable
      Objects <https://runestone.academy/ns/books/published/mesa_python/Functions/PassingMutableObjects.html>`__

-  `13. Tuple Packing and
   Unpacking <https://runestone.academy/ns/books/published/mesa_python/Tuples/toctree.html>`__

   -  `13.2. Tuple
      Packing <https://runestone.academy/ns/books/published/mesa_python/Tuples/TuplePacking.html>`__
   -  `13.3. Tuple Assignment with
      Unpacking <https://runestone.academy/ns/books/published/mesa_python/Tuples/TupleAssignmentwithunpacking.html>`__
   -  `13.4. Tuples as Return
      Values <https://runestone.academy/ns/books/published/mesa_python/Tuples/TuplesasReturnValues.html>`__
   -  `13.5. Unpacking Tuples as Arguments to Function
      Calls <https://runestone.academy/ns/books/published/mesa_python/Tuples/UnpackingArgumentsToFunctions.html>`__

-  `14. More About
   Iteration <https://runestone.academy/ns/books/published/mesa_python/MoreAboutIteration/toctree.html>`__

   -  `14.2. The ``while``
      Statement <https://runestone.academy/ns/books/published/mesa_python/MoreAboutIteration/ThewhileStatement.html>`__
   -  `14.3. The Listener
      Loop <https://runestone.academy/ns/books/published/mesa_python/MoreAboutIteration/listenerLoop.html>`__
      - `14.3.1.1. Sentinel
      Values <https://runestone.academy/ns/books/published/mesa_python/MoreAboutIteration/listenerLoop.html#sentinel-values>`__
      - `14.3.1.2. Validating
      Input <https://runestone.academy/ns/books/published/mesa_python/MoreAboutIteration/listenerLoop.html#validating-input>`__
   -  `14.4. Randomly Walking
      Turtles <https://runestone.academy/ns/books/published/mesa_python/MoreAboutIteration/RandomlyWalkingTurtles.html>`__
   -  `14.5. Break and
      Continue <https://runestone.academy/ns/books/published/mesa_python/MoreAboutIteration/BreakandContinue.html>`__
   -  `14.6. 👩🏾🖥️ Infinite
      Loops <https://runestone.academy/ns/books/published/mesa_python/MoreAboutIteration/WPInfiniteLoops.html>`__

Advanced Python programming
~~~~~~~~~~~~~~~~~~~~~~~~~~~

-  `15. Advanced
   Functions <https://runestone.academy/ns/books/published/mesa_python/AdvancedFunctions/toctree.html>`__

   -  `15.2. Keyword
      Parameters <https://runestone.academy/ns/books/published/mesa_python/AdvancedFunctions/KeywordParameters.html>`__
   -  `15.3. Anonymous functions with lambda
      expressions <https://runestone.academy/ns/books/published/mesa_python/AdvancedFunctions/Anonymousfunctionswithlambdaexpressions.html>`__
   -  `15.4. 👩🏾🖥️ Programming With
      Style <https://runestone.academy/ns/books/published/mesa_python/AdvancedFunctions/ProgrammingWithStyle.html>`__
   -  `15.5. Method
      Invocations <https://runestone.academy/ns/books/published/mesa_python/AdvancedFunctions/MethodInvocations.html>`__

-  `16.
   Sorting <https://runestone.academy/ns/books/published/mesa_python/Sorting/toctree.html>`__

   -  `16.2. Optional reverse
      parameter <https://runestone.academy/ns/books/published/mesa_python/Sorting/Optionalreverseparameter.html>`__
   -  `16.3. Optional key
      parameter <https://runestone.academy/ns/books/published/mesa_python/Sorting/Optionalkeyparameter.html>`__
   -  `16.4. Sorting a
      Dictionary <https://runestone.academy/ns/books/published/mesa_python/Sorting/SortingaDictionary.html>`__
   -  `16.5. Breaking Ties: Second
      Sorting <https://runestone.academy/ns/books/published/mesa_python/Sorting/SecondarySortOrder.html>`__
   -  `16.6. 👩🏾🖥️ When to use a Lambda
      Expression <https://runestone.academy/ns/books/published/mesa_python/Sorting/WPWhenToUseLambdaVsFunction.html>`__

-  `17. Nested Data and Nested
   Iteration <https://runestone.academy/ns/books/published/mesa_python/NestedData/toctree.html>`__

   -  `17.2. Nested
      Dictionaries <https://runestone.academy/ns/books/published/mesa_python/NestedData/NestedDictionaries.html>`__
   -  `17.3. Processing JSON
      results <https://runestone.academy/ns/books/published/mesa_python/NestedData/jsonlib.html>`__
   -  `17.4. Nested
      Iteration <https://runestone.academy/ns/books/published/mesa_python/NestedData/NestedIteration.html>`__
   -  `17.5. 👩🏾🖥️ Structuring Nested
      Data <https://runestone.academy/ns/books/published/mesa_python/NestedData/WPStructuringNestedData.html>`__
   -  `17.6. Deep and Shallow
      Copies <https://runestone.academy/ns/books/published/mesa_python/NestedData/DeepandShallowCopies.html>`__
   -  `17.7. 👩🏾🖥️ Extracting from Nested
      Data <https://runestone.academy/ns/books/published/mesa_python/NestedData/WPExtractFromNestedData.html>`__

-  `18. Test
   Cases <https://runestone.academy/ns/books/published/mesa_python/TestCases/toctree.html>`__

   -  `18.2. Checking Assumptions About Data
      Types <https://runestone.academy/ns/books/published/mesa_python/TestCases/TestingTypes.html>`__
   -  `18.3. Checking Other
      Assumptions <https://runestone.academy/ns/books/published/mesa_python/TestCases/TestingTypes.html#checking-other-assumptions>`__
   -  `18.5. Testing
      Loops <https://runestone.academy/ns/books/published/mesa_python/TestCases/TestingLoops.html>`__
   -  `18.6. Writing Test Cases for
      Functions <https://runestone.academy/ns/books/published/mesa_python/TestCases/Testingfunctions.html>`__
   -  `18.7. Testing Optional
      Parameters <https://runestone.academy/ns/books/published/mesa_python/TestCases/TestingOptionalParameters.html>`__

-  `19.
   Exceptions <https://runestone.academy/ns/books/published/mesa_python/Exceptions/toctree.html>`__

   -  `19.2. Exception Handling
      Flow-of-control <https://runestone.academy/ns/books/published/mesa_python/Exceptions/intro-exceptions.html#exception-handling-flow-of-control>`__
   -  `19.3. 👩🏾🖥️ When to use
      try/except <https://runestone.academy/ns/books/published/mesa_python/Exceptions/using-exceptions.html>`__
   -  `19.4. Standard
      Exceptions <https://runestone.academy/ns/books/published/mesa_python/Exceptions/standard-exceptions.html>`__

-  `20. Defining your own
   Classes <https://runestone.academy/ns/books/published/mesa_python/Classes/toctree.html>`__

   -  `20.3. User Defined
      Classes <https://runestone.academy/ns/books/published/mesa_python/Classes/UserDefinedClasses.html>`__
   -  `20.4. Adding Parameters to the
      Constructor <https://runestone.academy/ns/books/published/mesa_python/Classes/ImprovingourConstructor.html>`__
   -  `20.5. Adding Other Methods to a
      Class <https://runestone.academy/ns/books/published/mesa_python/Classes/AddingOtherMethodstoourClass.html>`__
   -  `20.8. Instances as Return
      Values <https://runestone.academy/ns/books/published/mesa_python/Classes/InstancesasReturnValues.html>`__
   -  `20.9. Sorting Lists of
      Instances <https://runestone.academy/ns/books/published/mesa_python/Classes/sorting_instances.html>`__
   -  `20.13. A Tamagotchi
      Game <https://runestone.academy/ns/books/published/mesa_python/Classes/Tamagotchi.html>`__

-  `21. Building
   Programs <https://runestone.academy/ns/books/published/mesa_python/BuildingPrograms/toctree.html>`__

   -  `21.1. Building A Program: A
      Strategy <https://runestone.academy/ns/books/published/mesa_python/BuildingPrograms/TheStrategy.html>`__
   -  `21.2. 👩🏾🖥️ Sketch an
      Outline <https://runestone.academy/ns/books/published/mesa_python/BuildingPrograms/WPSketchanOutline.html>`__
   -  `21.3. 👩🏾🖥️ Code one section at a
      time <https://runestone.academy/ns/books/published/mesa_python/BuildingPrograms/WPCodeSectionataTime.html>`__
   -  `21.4. 👩🏾🖥️ Clean
      Up <https://runestone.academy/ns/books/published/mesa_python/BuildingPrograms/WPCleanCode.html>`__

-  `22.
   Inheritance <https://runestone.academy/ns/books/published/mesa_python/Inheritance/toctree.html>`__

   -  `22.2. Inheriting Variables and
      Methods <https://runestone.academy/ns/books/published/mesa_python/Inheritance/inheritVarsAndMethods.html>`__
   -  `22.3. Overriding
      Methods <https://runestone.academy/ns/books/published/mesa_python/Inheritance/OverrideMethods.html>`__
   -  `22.4. Invoking the Parent Class’s
      Method <https://runestone.academy/ns/books/published/mesa_python/Inheritance/InvokingSuperMethods.html>`__
   -  `22.5. Tamagotchi
      Revisited <https://runestone.academy/ns/books/published/mesa_python/Inheritance/TamagotchiRevisited.html>`__
   -  `22.8. Project - Wheel of
      Python <https://runestone.academy/ns/books/published/mesa_python/Inheritance/chapterProject.html>`__

-  `23. More on Accumulation: Map, Filter, List Comprehension, and
   Zip <https://runestone.academy/ns/books/published/mesa_python/AdvancedAccumulation/toctree.html>`__

   -  `23.2.
      Map <https://runestone.academy/ns/books/published/mesa_python/AdvancedAccumulation/map.html>`__
   -  `23.3.
      Filter <https://runestone.academy/ns/books/published/mesa_python/AdvancedAccumulation/filter.html>`__
   -  `23.4. List
      Comprehensions <https://runestone.academy/ns/books/published/mesa_python/AdvancedAccumulation/listcomp.html>`__
   -  `23.5.
      Zip <https://runestone.academy/ns/books/published/mesa_python/AdvancedAccumulation/zip.html>`__
   -  `23.7. Chapter
      Assessment <https://runestone.academy/ns/books/published/mesa_python/AdvancedAccumulation/ChapterAssessment.html>`__
