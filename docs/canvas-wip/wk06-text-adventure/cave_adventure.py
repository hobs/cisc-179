# Room 0
room0 = "WELCOME TO ADVENTURE!!"
# Room 0 question
room0_question = "WOULD YOU LIKE INSTRUCTIONS? "
# room_answers is list of pairs of possible_answers and next_room_num
# an empty list means that room is unreachable from this room, e.g. room0 is unreachable from room0
room0_answers = [
    [],
    ["yes", "y", "yep", "si"],
    ["no", "nope", "nah", ""],
]

# Room 1 (instructions)
# Room 1 does not have a question because it automatically returns to start room question
room1 = "In each room, you can type commands like 'look', 'east', 'talk', 'eat', and 'fight'."
# If you specify an empty room#_question or room#_answers list, the game will return to the previous room immediately
room1_question = "Practice entering commands by saying 'look' here: "
room1_answers = ['look', 'l', 'lk', 'lok', 'examine']

# Room 2
room2 = "YOU ARE STANDING AT THE END OF A ROAD BEFORE A SMALL BRICK BUILDING. AROUND YOU IS A FOREST. ..."
# Question for Room 1, can be reused in other rooms
room2_question = "WHAT DO YOU WANT TO DO? "
room2_answers = [
    [],  # not possible to go back to room0 (welcome)
    [],  # not possible to go back to room1 (instructions)
    ["look", "l", "lk", "lok"],  #
    ["east", "e", "est"],
    ["west", "w", 'wes', "wst"],
    ["north", "n", "nrth", "nth"],
    ["south", "s", "sth"],
]

rooms = [
    [room0, room0_question],
    [room1, room1_question],
    [room2, room2_question],
]
all_room_answers = [
    room0_answers,
    room1_answers,
    room2_answers,
]

# Start in room0 (first pair of strings in the list of rooms)
current_room_num = 0
################################################
# Start room 0

# the first half of the room [description, question] pair is at index `[0]`
description = rooms[current_room_num][0]
# the second half of the room [description, question] pair is at index `[1]`
question = rooms[current_room_num][1]
print(description)
answer = input(question)

# def find_answer(this_room_answers=all_room_answers[current_room_num], answer=answer):
# If user does not enter text for possible answers then game will return to current_room_num room

next_room_answers = all_room_answers[current_room_num]
for i in range(len(next_room_answers)):
    print("DEBUG i next_room_answers[i]: ", i, next_room_answers[i])
    # [0] is for list of answer strings, [1] is for next room integer
    if answer in next_room_answers[i]:
        print("DEBUG i answer next_room_answers[i]: ", i, next_room_answers[i])
        current_room_num = i  # next_room_answers[i][1]  # this will be the next room
        break
print("DEBUG: current_room_num", current_room_num)
# End room 0
################################################

################################################
# Next room (may be room 0, room 1, or room 3)

# the first half of the room [description, question] pair is at index `[0]`
description = rooms[current_room_num][0]
# the second half of the room [description, question] pair is at index `[1]`
question = rooms[current_room_num][1]
print(description)
answer = input(question)


# End next room
#################################################
