#!/usr/bin/env python
# coding: utf-8

# In[1]:


print('Welcome to Castle Text Adventure')
print('Would you like instructions?')
answer = input('Yes or No?')
if answer == 'Yes':
    print('Here you go!')
    print('Instructions: Your selections affect the outcome of the game, so choose wisely.')
if answer == 'No':
    print('Here they are anyway.')
    print('Instructions: Your selections affect the outcome of the game, so choose wisely.')
else:
    quit()
    
start = input('Do you wish to start your journey? Yes or No?')
if start == 'Yes':
    print('Your mission is to destroy Dracula.')
if start == 'No':
    quit()
else:
    quit()

print('You arrive at the castle gates where an aura of evil blankets the castle.')
print('Would you like to go into the front door or take a shortcut to the moat area?')
option1 = input("Select your choice by typing either 'front' or 'moat'.")
if option1 == 'moat':
    print('Chains wrap around your limbs. The area fills with water and you drown. Game over.')
    quit()
if option1 == 'front':
    print('You arrive into the main hall where Lilith awaits you. Do you:')
    print('Throw holy water at Lilith to try to kill her OR Use your .45 handgun to shoot her heart?')
    option2 = input("Select your choice by typing either 'holy water' or 'handgun'.")
    if option2 == 'holy water':
        print('She blocks the holy water with her wings and then decapitates you. Game over.')
        quit()
    if option2 == 'handgun':
        print('You kill her instantly and head to Dracula\'s throne room.')
        print('After defeating Lilith, you enter Dracula\'s throne room. There is a puzzle you must solve in order to reach the secret area.')
        print('Do you solve the puzzle using the levers or trying to open the door.')
        option3 = input("Select your choice by typing either 'levers' or 'door'.")
        if option3 == 'levers':
            print('They are a trap. Acid pours down onto you and you cannot escape. Game over.')
            quit()
        if option3 == 'door':
            print('You enter a room full of mirrors.')
            print('Do you break the mirrors with your whip or use holy water to douse them?')
            option4 = input("Select your choice by typing either 'holy water' or 'whip'.")
            if option4 == 'whip':
                print('You start to break the mirrors with your whip, but a trap door opens and you fall to your death. Game over.')
                quit()
            if option4 == 'holy water':
                print('Dracula screams and then appears for a final battle.')
                print('You have three weapons at your disposal to try and defeat Dracula: holy water, handgun, and whip. Which do you use?')
                option5 = input("Select your weapon by typing either 'holy water', 'handgun', or 'whip'.")
                if option5 == 'holy water':
                    print('Dracula dies slowly but not before killing you. Mission accomplished but at a terrible cost.')
                    quit()
                if option5 == 'handgun':
                    print('Dracula melts your gun and slashes your throat, causing you to bleed to death. Game over.')
                if option5 == 'whip':
                    print('You use your infused whip and destroy Dracula after an intense battle. Mission accomplished!')
                    quit()


# In[ ]:




