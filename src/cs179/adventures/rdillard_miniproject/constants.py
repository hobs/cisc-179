#   Room names
BARRACKS = 'Barracks'
CHAPEL = 'Chapel'
EAST_CELL = 'East Cell'
EAST_CORRIDOR = 'East Corridor'
EMPRESS_KEEP = 'Empress\'s Keep'
GRAVEYARD = 'Graveyard'
NORTHEAST_CORRIDOR = 'Northeast Corridor'
PROVING_GROUND = 'Proving Ground'
SPIRE = 'Spire'
STAIRCASE = 'Staircase'
STARTING_ROOM = 'Starting Room'
THRONE_ROOM = 'Throne Room'
WEST_CELL = 'West Cell'
WEST_CORRIDOR_NORTH = 'West Corridor - North'
WEST_CORRIDOR_SOUTH = 'West Corridor - South'

#   Item names
SWORD = "Doombearer"
LIGHT_ARROW = "Skypiercer"
SHIELD = "Dark Carapace"
CLOAK = "Vermin's Shade"
BOMB = "Explosives"
SHOVEL = "Undertaker's Shovel"
POWER_UP_ONE = "Emperor's Might"
POWER_UP_TWO = "Empress's Ferocity"
KEY_ONE = "Mausoleum Key"
KEY_TWO = "Empress's Keep Key"


#   Enemy types: Empress is the only type implemented for now.
ZOMBIE = 'Zombie'
SKELETON = 'Skeleton'
GHOUL = 'Ghoul'
SYCOPHANT = 'Sycophant'
EMPRESS = 'The Empress'

#   Empress Attacks
FIRST_ATTACK = 'Back-handed Slash'
SECOND_ATTACK = 'Thrusting Stab'
START_THIRD_ATTACK = 'Charging'
FINISH_THIRD_ATTACK = 'Weight of the World'

#   Colors
RED = '\033[31m'
ORANGE = '\033[38;5;208m'
YELLOW = '\033[33m'
GREEN = '\033[32m'
BLUE = '\033[34m'
INDIGO = '\033[38;5;69m'
VIOLET = '\033[35m'
RESET = '\033[0m'
