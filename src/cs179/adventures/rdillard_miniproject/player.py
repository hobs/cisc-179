import constants as c


class Player:
    def __init__(self, sword, light_arrow, shield, cloak, shovel, bomb, power_up_one, power_up_two, key_one, key_two):
        self.sword = sword
        self.light_arrow = light_arrow
        self.shield = shield
        self.cloak = cloak
        self.shovel = shovel
        self.bomb = bomb
        self.power_up_one = power_up_one
        self.power_up_two = power_up_two
        self.key_one = key_one
        self.key_two = key_two
        self.health = 500
        self.inventory_empty = True

    def active_items(self):
        num_items = 0
        if self.inventory_empty:
            print(f"{c.RED}You have nothing in your inventory.{c.RESET}")
        if self.sword.active:
            num_items += 1
            print(f'- {self.sword.name}')
        if self.light_arrow.active:
            num_items += 1
            print(f'- {self.light_arrow.name}')
        if self.shield.active:
            num_items += 1
            print(f'- {self.shield.name}')
        if self.bomb.active:
            num_items += 1
            print(f'- {self.bomb.name}')
        if self.cloak.active:
            num_items += 1
            print(f'- {self.cloak.name}')
        if self.shovel.active:
            num_items += 1
            print(f'- {self.shovel.name}')
        if self.power_up_one.active:
            num_items += 1
            print(f'- {self.power_up_one.name}')
        if self.power_up_two.active:
            num_items += 1
            print(f'- {self.power_up_two.name}')
        if self.key_one.active:
            num_items += 1
            print(f'- {self.key_one.name}')
        if self.key_two.active:
            num_items += 1
            print(f'- {self.key_two.name}')
        return num_items

    def stats(self):
        print("\nPlayer stats:")
        print(f"Health: {c.RED}{self.health}{c.RESET}")
        if self.sword.active:
            if self.power_up_one.active or self.power_up_two.active:
                if self.power_up_one.active and self.power_up_two.active:
                    print(f"{self.sword.name}'s damage has been increased by {self.power_up_one.name} and {self.power_up_two.name}.")
                elif self.power_up_one.active and not self.power_up_two.active:
                    print(f"{self.sword.name}'s damage has been increased by {self.power_up_one.name}.")
                else:
                    print(f"{self.sword.name}'s damage has been increased by {self.power_up_two.name}.")
            print(f"Damage Infliction by {c.YELLOW}{self.sword.name}{c.RESET}: {c.BLUE}{self.sword.damage}{c.RESET}")
        if self.light_arrow.active:
            print(f"Damage infliction by {c.YELLOW}{self.light_arrow.name}{c.RESET}: {c.BLUE}{self.light_arrow.damage}{c.RESET}")
        if self.shield.active:
            print(f"Damage reduction by {c.YELLOW}{c.SHIELD}{c.RESET}: {c.BLUE}{self.shield.defense}{c.RESET}")
        if self.cloak.active:
            print(f"{c.YELLOW}{c.CLOAK}{c.RESET} will aid your other gear in battle:\n"
                  f"- {c.YELLOW}{c.SWORD}{c.RESET}: double damage every 6th round\n"
                  f"- {c.YELLOW}{c.LIGHT_ARROW}{c.RESET} heal by 25 hp every 5th round\n"
                  f"- {c.YELLOW}{c.SHIELD}{c.RESET} prevent all damage every 12th round\n")

    def pick_up_item(self, item_name):
        self.inventory_empty = False
        if item_name == self.sword.name:
            self.sword.active = True
        if item_name == self.shield.name:
            self.shield.active = True
        if item_name == self.light_arrow.name:
            self.light_arrow.active = True
        if item_name == self.bomb.name:
            self.bomb.active = True
        if item_name == self.cloak.name:
            self.cloak.active = True
            self.shield.defense *= 5
        if item_name == self.shovel.name:
            self.shovel.active = True
        if item_name == self.power_up_one.name:
            self.power_up_one.active = True
            self.sword.damage *= self.sword.damage
        if item_name == self.power_up_two.name:
            self.power_up_two.active = True
            self.sword.damage *= self.sword.damage
        if item_name == self.key_one.name:
            self.key_one.active = True
        if item_name == self.key_two.name:
            self.key_two.active = True
