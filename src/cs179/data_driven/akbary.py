def is_palindrome(s):
    s = s.replace(" ", "").lower()
    return s == s[::-1]


def load_rooms(file_path):
    rooms = {}
    with open(file_path, 'r') as file:
        lines = file.readlines()

    current_room = None
    for line in lines:
        line = line.strip()
        if line.startswith("Room"):
            current_room = line
            rooms[current_room] = {"description": "", "options": [], "correct_answer": None}
        elif current_room and not rooms[current_room]["description"]:
            rooms[current_room]["description"] = line
        elif line.startswith("Correct answer:"):
            rooms[current_room]["correct_answer"] = int(line.split(":")[1].strip())
        elif current_room:
            rooms[current_room]["options"].append(line)

    return rooms


def main():
    rooms = load_rooms('akbary.txt')
    current_room = 'Room 1'
    score = 0
    missed = 0
    while current_room:
        room_data = rooms[current_room]
##the palindrome case
        if current_room == "Room 3":
            print(room_data["description"])
            pal = input("Please enter a palindrome:")
            if is_palindrome(pal):
                current_room_number = int(current_room.split()[1]) + 1
                current_room = f'Room {current_room_number}' if f'Room {current_room_number}' in rooms else None
                score += 1
            else:
                print("Invalid choice, try again.")
                missed += 1
##the rest of the rooms
        else:
            print(room_data["description"])
            for option in room_data["options"]:
                print(option)

            choice = input("Choose an option: ")
            if choice.isdigit() and int(choice) == room_data["correct_answer"]:
                current_room_number = int(current_room.split()[1]) + 1
                current_room = f'Room {current_room_number}' if f'Room {current_room_number}' in rooms else None
                score += 1
            else:
                print("Invalid choice, try again.")
                missed += 1
    print(f"Congratulations, you are free! you have reached the score of {score} and missed {missed}.")


if __name__ == "__main__":
    main()
