# lane.py
# Read the data file (I used a plain text format call markdown to make it a little more readable)


def read_markdown(path='lane.md'):
    """Read and parse a markdown text file and return a graph (dict of dicts) for the rooms """
    with open(path) as f:
        lines = f.readlines()

    graph = {}
    first_room = None
    for line in lines:
        # I created 3 kinds of lines in the markdown text file:
        #   1. The state name starts with "#" and contains the outer key (room or state name) for your nested dict of dicts
        #   2. The command lines that start with "?: ".
        #      This gives you the keys (start character of user commands) and values (state names) for the inner dict
        #   3. Lines without any special pattern at the beginning.
        #      This gives you the text description of the room that you can store in the inner dictionary with a special key, "description."

        # 1. Lines starting with "#" contain the outer dict key (room name)
        if line[0] == '#':
            key = line.strip('#').strip().lower()
            graph[key] = {}
            if first_room is None:
                first_room = key
        # 2. Lines starting with a single character and a colon are for commands and the destination room
        elif line[1] == ':':
            graph[key][line[0]] = line[2:].strip().lower()
        # 3. Lines without any special pattern at the beginning are for room description text
        else:
            graph[key]['description'] = line.strip()

    return graph, first_room


def play_game(graph, first_room):
    """Print room descriptions and move to the next room depending on user input"""
    # Our game starts in the room (state) named "morning"
    room = first_room
    visited_rooms = []

    while room:
        visited_rooms.append(room)
        print(graph[room]['description'])
        if len(graph[room]) > 1:
            command = ' '
            while command not in graph[room]:
                command = input('>>> ')
            room = graph[room][command]
        else:
            room = ''

    # Return the list of visited rooms so you can tell whether the player won or not
    return visited_rooms


if __name__ == "__main__":
    graph, first_room = read_markdown('lane.md')
    rooms = play_game(graph, first_room=first_room)
    print(f'You {"won" if "win" in rooms else "lost"} after visiting these {len(rooms)} rooms: {rooms}.')
