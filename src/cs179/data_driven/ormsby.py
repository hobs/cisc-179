### This is a Runescape text Adventure based game with 5 rooms.
### The player can move between rooms and interact with the options.

print("Welcome to Runescape!")
print("You log into the game and your character loads into Gielinor.")
print("To begin your daily adventure, you decide to explore the continent.")
print("You walk into the town of Gielinor and decide to go to the local tavern.")
print("Which adventure would you like to begin with, choose one of five?")
print("Varrock Tavern, King of Varrock, Demon Impling, Fishing, or Dwarven Mines?")

city_choice = input("> ")

if city_choice.lower() == "varrock tavern":
    print("You sit down at the table and order a drink.")
    print("As you are drinking, you hear a strange noise coming from the closet.")
    investigate_choice = input("Do you decide to investigate the noise? (yes/no) ")

    if investigate_choice.lower() == "yes":
        print("You open the closet and find a strange creature.")
        print("He tells you that he has been gnome-napped!")
        print("He asks you to take him back to the Tree Gnome Village.")
    elif investigate_choice.lower() == "no":
        print("You decide not to investigate the noise.")
        print("You leave the tavern and continue your adventure.")
    else:
        print("Invalid choice. Please enter yes or no.")

elif city_choice.lower() == "king of varrock":
    print("The king asks you how your day went.")
    print("As you are talking to him, you see a lady get pickpocketed.")
    save_choice = input("Do you save the lady? (yes/no) ")

    if save_choice.lower() == "yes":
        print("You run over and fight the thief!")
    elif save_choice.lower() == "no":
        print("You decide it is not your business.")
        print("You leave the castle and continue on your adventure.")
    else:
        print("Invalid choice. Please enter yes or no.")

elif city_choice.lower() == "demon impling":
    print("You see a demon impling flying around! Do you chase it?")
    imp_choice = input("(yes/no) ")

    if imp_choice.lower() == "yes":
        print("You chase the demon impling and catch it!")
    elif imp_choice.lower() == "no":
        print("There are probably better ways to make gold, I guess...")
        print("You leave Varrock and continue on your adventure.")
    else:
        print("Invalid choice. Please enter yes or no.")

elif city_choice.lower() == "fishing":
    print("You decide to train your fishing.")
    fishing_choice = input("Do you want to train your fishing? (yes/no) ")

    if fishing_choice.lower() == "yes":
        print("You grab your fly fishing rod and feathers and head to the creek.")
        print("You see a man fishing and you decide to ask him if he wants to join you.")
    elif fishing_choice.lower() == "no":
        print("You decide not to train your fishing.")
        print("You leave the Barbarian Village and continue on your adventure.")
    else:
        print("Invalid choice. Please enter yes or no.")

elif city_choice.lower() == "dwarven mines":
    print("You decide to go to the Dwarven Mines.")
    print("You see a man and you decide to ask him if he wants to join you.")
    dwarf_choice = input("(yes/no) ")

    if dwarf_choice.lower() == "yes":
        print("You approach a man in the Dwarven Mines and ask him to join you.")
        print("The man steals your things inside the mine!")
    elif dwarf_choice.lower() == "no":
        print("You go mine Coal Ore in the Dwarven Mines.")
        print("You finish mining for the day and log out.")
    else:
        print("Invalid choice. Please enter yes or no.")

elif city_choice.lower() == "Party Room":
    print("You decide to go to the Party Room.")
    print("You ask the people inside if they're doing a drop party")
    dwarf_choice = input("(yes/no) ")

    if dwarf_choice.lower() == "yes":
        print("You join the drop party")
        print("You recieve a Rune Plate body")
    elif dwarf_choice.lower() == "no":
        print("You leave the Party Room.")
        print("You continue your adventure.")
    else:
        print("Invalid choice. Please enter yes or no.")